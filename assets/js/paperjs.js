function onKeyDown(event) {
	var maxPoint = new Point(view.size.width, view.size.height);
	var randomPoint = Point.random();
	var point = maxPoint * randomPoint; 
	var circle = new Path.Circle(point, 500).fillColor = 'red';
}

// The onFrame function is called up to 60 times a second:
function onFrame(event) {
	// Run through the active layer's children list and change
	// the position of the placed symbols:
	for (var i = 0; i < project.activeLayer.children.length; i++) {
		var item = project.activeLayer.children[i];
		item.scale(.9);
		item.fillColor.hue += 1;
	}
}